# Specify modules in bower not to wrap in definition
nowrap = 'jquery|gsap|tweakjs|eframe|normalize.stylus'

exports.config =

  watched: ['app', 'test', 'vendor', 'bower_components']

  conventions:
    vendor: ///(^node_modules|vendor|bower_components[\/\\](?!#{nowrap}).*)///

  modules:
    nameCleaner: (path) ->
      path = path.replace /\./g, '/'
      path.replace /^(app|app[\/\\]components|bower_components)[\/\\]/, ''

  files:
    javascripts:
      defaultExtension: 'coffee'
      joinTo:
        'javascripts/app.js': /^bower_components|vendor|(app(?!.*production-config).*)+/
      order:
        before: [
          'components/**/*.*',
          'app/**/*.*'
        ]

    stylesheets:
      defaultExtension: 'styl'
      joinTo:
        'styles/app.css': /^bower_components|vendor|app/
      order:
        before: [
          'bower_components/normalize.styl/*.*'
          'components/**/*.*',
          'app/styles/**/*.*'
          'app/**/*.*'
        ]

    templates:
      defaultExtension: 'hbs'
      joinTo:
        'javascripts/app.js': /^bower_components|vendor|app/
      before: [
        'bower_components/**/*.*',
        'components/**/*.*',
        'app/**/*.*'
      ]

  server:
    port: 3333
  
  plugins:
    stylus:
      imports: [
        'nib'
      ]
  
  overrides:
    production:
      plugins: autoreload: enabled: false
      optimize: true
      sourceMaps: false
      files:
        javascripts:
          joinTo:
            'javascripts/app.js': /^bower_components|vendor|app/